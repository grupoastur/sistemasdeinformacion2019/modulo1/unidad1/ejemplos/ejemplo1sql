﻿-- Ejemplo de creación de una base de datos de veterinarios
-- tiene tres tablas

DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;

/*
esto es un comentario de como estamos creando la tabla de animales
  */
CREATE TABLE animales(
id int AUTO_INCREMENT,
  nombre varchar(100),
  raza varchar(100),
  fechaNac date,
  PRIMARY KEY(id)
);

CREATE TABLE veterinarios(
  cod int AUTO_INCREMENT,
  nombre varchar(100),
  especialidad varchar(100),
  PRIMARY KEY(cod)
  );

CREATE TABLE acuden(
  idanimal int,
  codVet int,
  fecha date,
  PRIMARY KEY (idanimal,codVet,fecha), -- creando la clave principal
  UNIQUE KEY(idanimal),
  CONSTRAINT fkacudenanimal FOREIGN KEY (idanimal)REFERENCES animales (id),
  CONSTRAINT fkacudeveterinarios FOREIGN KEY(codVet)REFERENCES veterinarios(cod)
  );


-- insertar un registro
INSERT INTO animales ( nombre, raza, fechaNac)
  VALUE ('jorge','buldog','2000/2/1'),
  ('ana','caniche','2003/6/3');

-- listar
  SELECT * FROM animales;